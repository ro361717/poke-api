package com.example.apppokemonapi

class Pokemon {

    private var number = 0
    private var name: String? = null
    private var url: String? = null

    fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }

    fun getUrl(): String? {
        return url
    }

    fun setUrl(url: String?) {
        this.url = url
    }

    fun getNumber(): Int {
        val urlPartes = url!!.split("/".toRegex()).toTypedArray()
        return urlPartes[urlPartes.size - 1].toInt()
    }

    fun setNumber(number: Int) {
        this.number = number
    }

}
