package com.example.apppokemonapi

import retrofit2.http.GET
import retrofit2.http.Query


interface PokeapiService {
    @GET("pokemon")
    fun obtenerListaPokemon(@Query("limit") limit:Int, @Query("offset") offset:Int): retrofit2.Call<PokemonRespuesta>
}